# SPACEX FALCON 9 LAUNCH SYSTEM DATABASE

### INTRODUCCIÓN
Esta base de datos relacional representa la actividad de la compañía aeroespacial SpaceX en cuanto a las misiones que
utilizan los cohetes Falcon 9 se refiere. Con 4 tablas, una de ellas intermedia entre booster y landing_site y otra de
mission, he pretendido almacenar información sobre la vida útil de estos cohetes reutilizables, sus aterrizajes y las
misiones que han puesto en órbita.

Omito los boosters que han participado en misiones Falcon Heavy, ya que es un sistema de lanzamiento aparte y generaría
cierta heterogeneidad innecesaria. Además cabe considerar que la precisión de los datos no es total, ya que es difícil
encontrar las fuentes para según qué datos. Aun así agradezco enormemente al subreddit
r/SpaceX (https://www.reddit.com/r/spacex/) y por ende a su comunidad, los datos facilitados (los he sacado casi todos
de ahí, sublime es).

