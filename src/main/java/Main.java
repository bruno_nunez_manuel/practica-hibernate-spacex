import entity.*;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory =
                Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();

        // Debo decir que el automapping de IntelliJ es algo que ni comprendo ni funciona.
        // Por ello he mapeado cada entidad a mano, quizás mal, quizás bien, pero los insert van ok.
        // No entiendo por que hay que manejar las constraints en las @Entity si las SQLException y
        // los plugins de DBs para IDEs ya te dan bastante información fácilmente. En ese sentido he
        // pasado olímpicamente de poner annotations de FKs, PKs, compound PKs de FKs en tablas intermedias...
        // Por no hablar de problemas abajo expuestos de que en los rollback hace autoincrement de las
        // @GeneratedValues. Y eso de poner unique en @Table para un @Id? Es que los @Id no son unique?
        // Me recuerda a los create table con las PK puestas con unique muahahaha.
        // Y por cierto, por qué se usan setters para crear objetos en vez de constructors? Si funciona...
        try {
            transaction.begin();

            F9Booster f9Booster = new F9Booster();
            f9Booster.setBoosterId("B10zz");
            f9Booster.setFirstStaticFire(Date.valueOf("2021-05-22"));
            f9Booster.setTimesFlown(99);
            f9Booster.setIsOperative(1); // Lo he puesto en la entity como int. Viva.
            entityManager.persist(f9Booster);

            Landing landing = new Landing();
            landing.setLandingSiteId("LZ-1");
            landing.setBoosterId("B1051");
            landing.setLandingDate(Date.valueOf("2021-05-01"));
            entityManager.persist(landing);

            LandingSite landingSite = new LandingSite();
            landingSite.setLandingSiteId("rri2");
            landingSite.setName("Phobos Oil Rig (sí, van a aterrizar en una plataforma petrolífera)");
            landingSite.setLandOrSeaEnum(LandOrSeaEnum.sea);
            landingSite.setOperative(false);
            entityManager.persist(landingSite);

            Mission mission = new Mission();
            mission.setMissionId(41212); // Hay autoincrement oculto cuando hay rollback, así que paso de @GeneratedValue.
            mission.setName("Mars Onq");
            mission.setPayloadKg(9999.98);
            mission.setFlightDate(Date.valueOf("2021-05-22"));
            mission.setBoosterId("B1049");
            entityManager.persist(mission);

            transaction.commit();


            // Con obtener, no sé realmente qué pides. Probablemente estaba abstraído en clase e ignorando
            // esas cosas raras que hacías con XMLs... Según esto:
            // https://stackoverflow.com/questions/7147018/hibernate-annotation-or-xml-configuration
            // usar XMLs es neandertal, Annotations is the way (lo decía el vulog allá por 2009).
            // Fin de la cita.

            List<F9Booster> f9BoosterList =
                    entityManager.createQuery("FROM F9Booster", F9Booster.class).getResultList();
            for (F9Booster f : f9BoosterList) System.out.println(f);

            List<Landing> landingList =
                    entityManager.createQuery("FROM Landing", Landing.class).getResultList();
            for (Landing l : landingList) System.out.println(l);

            List<LandingSite> landingSiteList =
                    entityManager.createQuery("FROM LandingSite", LandingSite.class).getResultList();
            for (LandingSite ls : landingSiteList) System.out.println(ls);

            List<Mission> missionList =
                    entityManager.createQuery("FROM Mission", Mission.class).getResultList();
            for (Mission m : missionList) System.out.println(m);

        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }


    }
}
