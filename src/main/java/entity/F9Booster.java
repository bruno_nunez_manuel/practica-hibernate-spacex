package entity;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "f9_booster", schema = "spacex_f9")
public class F9Booster {
    @Id
    @Column (name = "booster_id")
    private String boosterId;
    @Column (name = "first_static_fire")
    private Date firstStaticFire;
    @Column (name = "times_flown")
    private int timesFlown;
    @Column (name = "is_operative")
    private int isOperative;

    public String getBoosterId() {
        return boosterId;
    }

    public void setBoosterId(String boosterId) {
        this.boosterId = boosterId;
    }

    public Date getFirstStaticFire() {
        return firstStaticFire;
    }

    public void setFirstStaticFire(Date firstStaticFire) {
        this.firstStaticFire = firstStaticFire;
    }

    public int getTimesFlown() {
        return timesFlown;
    }

    public void setTimesFlown(int timesFlown) {
        this.timesFlown = timesFlown;
    }

    public int getIsOperative() {
        return isOperative;
    }

    public void setIsOperative(int isOperative) {
        this.isOperative = isOperative;
    }

    @Override
    public String toString() {
        return "F9Booster{" +
                "boosterId='" + boosterId + '\'' +
                ", firstStaticFire=" + firstStaticFire +
                ", timesFlown=" + timesFlown +
                ", isOperative=" + isOperative +
                '}';
    }
}
