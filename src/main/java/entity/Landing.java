package entity;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table (name = "landing", schema = "spacex_f9")
public class Landing {
    @Id
    @Column (name = "landing_site_id")
    private String landingSiteId;
    @Column (name = "booster_id")
    private String boosterId;
    @Column (name = "landing_date")
    private Date landingDate;

    public String getLandingSiteId() {
        return landingSiteId;
    }

    public void setLandingSiteId(String landingSiteId) {
        this.landingSiteId = landingSiteId;
    }

    public String getBoosterId() {
        return boosterId;
    }

    public void setBoosterId(String boosterId) {
        this.boosterId = boosterId;
    }

    public Date getLandingDate() {
        return landingDate;
    }

    public void setLandingDate(Date landingDate) {
        this.landingDate = landingDate;
    }

    @Override
    public String toString() {
        return "Landing{" +
                "landingSiteId='" + landingSiteId + '\'' +
                ", boosterId='" + boosterId + '\'' +
                ", landingDate=" + landingDate +
                '}';
    }
}
