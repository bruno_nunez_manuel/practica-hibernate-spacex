package entity;

import javax.persistence.*;

@Entity
@Table(name = "landing_site", schema = "spacex_f9")
public class LandingSite {
    @Id
    @Column (name = "landing_site_id")
    private String landingSiteId;
    @Column (name = "name")
    private String name;
    @Column (name = "land_or_sea")
    @Enumerated(EnumType.STRING)
    private LandOrSeaEnum landOrSeaEnum;
    @Column (name = "is_operative", columnDefinition="tinyint(1) default 1")
    private boolean isOperative;

    public String getLandingSiteId() {
        return landingSiteId;
    }

    public void setLandingSiteId(String landingSiteId) {
        this.landingSiteId = landingSiteId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LandOrSeaEnum getLandOrSeaEnum() {
        return landOrSeaEnum;
    }

    public void setLandOrSeaEnum(LandOrSeaEnum landOrSeaEnum) {
        this.landOrSeaEnum = landOrSeaEnum;
    }

    public boolean isOperative() {
        return isOperative;
    }

    public void setOperative(boolean operative) {
        isOperative = operative;
    }
}
