package entity;

import javax.persistence.*;
import java.sql.Date;

// He cometido un error de diseño, debí haber creado más constraints para que no haya misiones iguales...

@Entity
@Table (name = "mission", schema = "spacex_f9") // Unique un @Id? Mucha redundante explicitud? No lo pongo.
public class Mission {
    @Id
    @Column (name = "mission_id")
    private int missionId;
    @Column (name = "name")
    private String name;
    @Column (name = "payload_kg")
    private Double payloadKg;
    @Column (name = "altitude_km")
    private int altitudeKm;
    @Column (name = "flight_date")
    private Date flightDate;
    @Column (name = "booster_id") // Ni idea de la FKs...
    private String boosterId;

    public int getMissionId() {
        return missionId;
    }

    public void setMissionId(int missionId) {
        this.missionId = missionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPayloadKg() {
        return payloadKg;
    }

    public void setPayloadKg(Double payloadKg) {
        this.payloadKg = payloadKg;
    }

    public int getAltitudeKm() {
        return altitudeKm;
    }

    public void setAltitudeKm(int altitudeKm) {
        this.altitudeKm = altitudeKm;
    }

    public Date getFlightDate() {
        return flightDate;
    }

    public void setFlightDate(Date flightDate) {
        this.flightDate = flightDate;
    }

    public String getBoosterId() {
        return boosterId;
    }

    public void setBoosterId(String boosterId) {
        this.boosterId = boosterId;
    }

    @Override
    public String toString() {
        return "Mission{" +
                "missionId=" + missionId +
                ", name='" + name + '\'' +
                ", payloadKg=" + payloadKg +
                ", altitudeKm=" + altitudeKm +
                ", flightDate=" + flightDate +
                '}';
    }
}
